# 0.1.1

- doc: terraform-docs update

# 0.1.0

- feat: adds secret manager, secret, policy and rotation

# 0.0.0

- Initial version
