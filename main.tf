locals {
  tags = merge(
    var.tags,
    {
      managed-by = "terraform"
    }
  )
}

####
# Secret
####

resource "aws_secretsmanager_secret" "this" {
  name                    = var.name
  description             = var.description
  kms_key_id              = var.kms_key_id
  recovery_window_in_days = var.recovery_window_in_days
  tags                    = local.tags
}

####
# Rotation
####

resource "aws_secretsmanager_secret_rotation" "this" {
  count = var.rotation_lambda_arn != null ? 1 : 0

  secret_id           = aws_secretsmanager_secret.this.id
  rotation_lambda_arn = var.rotation_lambda_arn

  rotation_rules {
    automatically_after_days = var.rotation_automatically_after_days
  }
}

####
# Version
####

locals {
  secret_string = var.secret_string != null ? var.secret_string : (
    var.secret_key_values != null ? jsonencode(var.secret_key_values) : ""
  )
}

resource "aws_secretsmanager_secret_version" "this" {
  count = var.secret_string != null || var.secret_key_values != null ? 1 : 0

  secret_id     = aws_secretsmanager_secret.this.id
  secret_string = local.secret_string

  lifecycle {
    ignore_changes = [
      secret_string
    ]
  }
}

####
# Policy
####

locals {
  policy_content = var.policy_content != null ? var.policy_content : (
    var.policy_allow_scope == "READ" ? element(concat(data.aws_iam_policy_document.read.*.json, [""]), 0) : (
      var.policy_allow_scope == "READ_WRITE" ? element(concat(data.aws_iam_policy_document.read_write.*.json, [""]), 0) : (
        var.policy_allow_scope == "ADMIN" ? element(concat(data.aws_iam_policy_document.admin.*.json, [""]), 0) : ""
      )
    )
  )
}

resource "aws_secretsmanager_secret_policy" "this" {
  count = var.policy_content != null || var.policy_allow_scope != "NONE" ? 1 : 0

  secret_arn = aws_secretsmanager_secret.this.arn

  policy = local.policy_content
}
