variable "tags" {
  default     = {}
  description = "Tags to be used by all resources of this module."
}

####
# Secret
####

variable "name" {
  default     = "secret"
  description = "Name of the secret to create."

  validation {
    condition     = 1 <= length(var.name) && length(var.name) <= 256
    error_message = "The var.name length must be between 1 and 256 characters."
  }
}

variable "description" {
  default     = ""
  description = "A description of the secret to create."

  validation {
    condition     = length(var.description) <= 2048
    error_message = "The var.description length must be between 0 and 2048 characters."
  }
}

variable "kms_key_id" {
  type        = string
  default     = null
  description = "Specifies the ARN or Id of the AWS KMS customer master key (CMK) to be used to encrypt the secret values in the versions stored in this secret. If you don't specify this value, then Secrets Manager defaults to using the AWS account's default CMK (the one named `aws/secretsmanager`). If the default KMS CMK with that name doesn't yet exist, then AWS Secrets Manager creates it for you automatically the first time."
}

variable "recovery_window_in_days" {
  default     = 30
  type        = number
  description = "Specifies the number of days that AWS Secrets Manager waits before it can delete the secret. This value can be `0` to force deletion without recovery or range from `7` to `30` days."

  validation {
    condition     = var.recovery_window_in_days == 0 || (7 <= var.recovery_window_in_days && var.recovery_window_in_days <= 30)
    error_message = "The var.recovery_window_in_days must be 0 or be between 7 and 30."
  }
}

####
# Rotation
####

variable "rotation_lambda_arn" {
  type        = string
  default     = null
  description = "Specifies the ARN of the Lambda function that can rotate the secret. If `null`, the secret will not be rotated."

  validation {
    condition     = var.rotation_lambda_arn == null || can(regex("^arn:aws(-us-gov|-cn)?:lambda:([a-z]{2}-[a-z]{4,10}-[1-9]{1})?:[0-9]{12}:function:[^:]{1,64}$", var.rotation_lambda_arn))
    error_message = "The var.rotation_lambda_arn must match “^arn:aws(-us-gov|-cn)?:lambda:([a-z0-9-]{6,16})?:[0-9]{12}:function:[^:]{1,64}$”."
  }
}

variable "rotation_automatically_after_days" {
  default     = 30
  description = "Specifies the number of days between automatic scheduled rotations of the secret. If `var.rotation_lambda_arn` is `null`, this will be ignored."

  validation {
    condition     = var.rotation_automatically_after_days == null || (1 <= var.rotation_automatically_after_days && var.rotation_automatically_after_days <= 1000)
    error_message = "The var.rotation_automatically_after_days length must be between 1 and 1000."
  }
}

####
# Version
####

variable "secret_string" {
  type        = string
  default     = null
  description = "Specifies text data that you want to encrypt and store in a new version of the secret. "
}

variable "secret_key_values" {
  type = map(string)

  default     = null
  description = "Specifies an object with key values to store as the secrets. If `var.secret_string` is not `null`, this variable is ignored."
}

####
# Policy
####

variable "policy_content" {
  type        = string
  default     = null
  description = "A valid JSON document representing a [resource policy](https://docs.aws.amazon.com/secretsmanager/latest/userguide/auth-and-access_resource-based-policies.html). For more information about building AWS IAM policy documents with Terraform, see the [AWS IAM Policy Document Guide](https://learn.hashicorp.com/terraform/aws/iam-policy)."
}

variable "policy_allow_scope" {
  type        = string
  default     = "NONE"
  description = "If `var.policy_content` is not set, this option will be read to generate a policy for the secret. The possible options: `READ`, `READ_WRITE`, `ADMIN` or `NONE`."

  validation {
    condition     = 0 == length(setsubtract([var.policy_allow_scope], ["READ", "READ_WRITE", "ADMIN", "NONE"]))
    error_message = "The var.policy_allow_scope is an unsupported value. Allowed: “READ”, “READ_WRITE”, “ADMIN” or “NONE”."
  }
}
