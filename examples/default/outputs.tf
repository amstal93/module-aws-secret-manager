output "default" {
  value = module.default
}

output "complete" {
  value = module.complete
}

output "no_secret" {
  value = module.no_secret
}
