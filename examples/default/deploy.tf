#####
# Context
#####

resource "random_string" "this" {
  length  = 6
  upper   = false
  special = false
}

resource "aws_kms_key" "test" {
  description             = "${random_string.this.result}tftest"
  deletion_window_in_days = 7
}

#####
# Default
#####

module "default" {
  source = "../../"

  name          = "${random_string.this.result}tftest"
  secret_string = "ThisIsASecret?"
}

#####
# Complete
#####

module "complete" {
  source = "../../"

  name                    = "${random_string.this.result}tftest2"
  description             = "Tftest secret"
  recovery_window_in_days = 0

  policy_allow_scope = "READ_WRITE"

  kms_key_id = aws_kms_key.test.id

  secret_key_values = {
    username = "coco"
    password = "chanel"
  }

  tags = {
    tftest = "tftest"
  }
}

#####
# No secret
#####

module "no_secret" {
  source = "../../"

  name = "${random_string.this.result}tftest3"
}
